package com.skripsi.monchat.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.skripsi.monchat.activity.ChatActivity
import com.skripsi.monchat.R
import com.skripsi.monchat.model.UserModel
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.Handle
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

import java.util.ArrayList

class AddFriendAdapter(private val mContext: Context, private var userModels: MutableList<UserModel>) :
    RecyclerView.Adapter<AddFriendAdapter.MyViewHolder>() {

    private lateinit var itemView: View
    private lateinit var session: Session
    private lateinit var cBack: Callback<ResponseBody>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_add_friend, parent, false)

        session = Session(mContext)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val userModel = userModels!![position]
        holder.tvName.text = userModel.name
        holder.tvEmail.text = userModel.email

        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, ChatActivity::class.java)
            intent.putExtra("USERNAME", userModel.username)
            intent.putExtra("NAME", userModel.name)
            mContext.startActivity(intent)
        }

        holder.btnAdd.setOnClickListener {
            addFriend(userModel.username)
        }
    }

    fun addFriend(username: String){
        val call = ParamReq.requestInviteFriendSQL(session.get("username")!!, username, mContext)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleAcceptFriendSQL(response.body()!!.string(), mContext)
                    if (handle) {
                        Toast.makeText(mContext, "Berhasil menambahkan teman", Toast.LENGTH_LONG).show()
                        notifyDataSetChanged()
                    } else {
                        Toast.makeText(mContext, "Gagal Terhubung ke MySQL", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(mContext, "Tidak ada Request Teman", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(mContext, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(mContext, call, cBack, false, "Loading")
    }

    override fun getItemCount(): Int {
        return userModels!!.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvName: TextView
        internal var tvEmail: TextView
        internal var btnAdd: Button

        init {

            tvName = itemView.findViewById(R.id.tv_name)
            tvEmail = itemView.findViewById(R.id.tv_email)
            btnAdd = itemView.findViewById(R.id.btn_add_friend)

        }
    }


    fun setFilter(newList: List<UserModel>) {
        userModels = ArrayList()
        userModels!!.addAll(newList)
        notifyDataSetChanged()
    }
}
