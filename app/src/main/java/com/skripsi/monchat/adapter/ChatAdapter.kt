package com.skripsi.monchat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.skripsi.monchat.model.ChatModel

import androidx.annotation.NonNull
import com.skripsi.monchat.R

class ChatAdapter(private val mContext: Context, private var chatModel: MutableList<ChatModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var itemView: View

    private val MY_MESSAGE = 1
    private val THEIR_MESSAGE = 2

    @NonNull
    override fun onCreateViewHolder(@NonNull viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        if (viewType == MY_MESSAGE) { // for call layout
            view = LayoutInflater.from(mContext).inflate(R.layout.my_message, viewGroup, false)
            return MyMessageHolder(view)

        } else { // for email layout
            view = LayoutInflater.from(mContext).inflate(R.layout.their_message, viewGroup, false)
            return TheirMessageHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val chat = chatModel[position]
        if (getItemViewType(position) == MY_MESSAGE) {
            (holder as MyMessageHolder).setMyMessage(chat)
        } else {
            (holder as TheirMessageHolder).setTheirMessage(chat)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (chatModel[position].isMine.equals("1")) {
            MY_MESSAGE
        } else {
            THEIR_MESSAGE
        }
    }

    override fun getItemCount(): Int {
        return chatModel!!.size
    }

    fun refreshView() {
        notifyDataSetChanged()
    }

    inner class MyMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var tvMessage: TextView

        init {
            tvMessage = itemView.findViewById(R.id.tv_message)
        }

        fun setMyMessage(chatModel: ChatModel) {
            tvMessage.text = chatModel.body
        }
    }

    inner class TheirMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var tvName: TextView
        lateinit var tvMessage: TextView
        lateinit var avatar: AppCompatImageView
        init {
            avatar = itemView.findViewById(R.id.avatar)
            tvName = itemView.findViewById(R.id.tv_name)
            tvMessage = itemView.findViewById(R.id.tv_message)
        }

        fun setTheirMessage(chatModel: ChatModel) {
            tvName.text = chatModel.senderName
            tvMessage.text = chatModel.body
        }
    }

}
