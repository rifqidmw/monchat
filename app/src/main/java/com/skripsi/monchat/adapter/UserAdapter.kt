package com.skripsi.monchat.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.skripsi.monchat.activity.ChatActivity
import com.skripsi.monchat.R
import com.skripsi.monchat.adapter.UserAdapter.MyViewHolder
import com.skripsi.monchat.model.UserModel
import com.skripsi.monchat.utils.Session

import java.util.ArrayList

class UserAdapter(private val mContext: Context, private var userModels: MutableList<UserModel>) :
    RecyclerView.Adapter<MyViewHolder>() {

    private lateinit var itemView: View

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val userModel = userModels!![position]
        val session = Session(mContext)
        holder.tvName.text = userModel.name
        holder.tvEmail.text = userModel.email

        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, ChatActivity::class.java)
            intent.putExtra("USERNAME", userModel.username)
            intent.putExtra("NAME", userModel.name)
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return userModels!!.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvName: TextView
        internal var tvEmail: TextView

        init {

            tvName = itemView.findViewById(R.id.tv_name)
            tvEmail = itemView.findViewById(R.id.tv_email)

        }
    }


    fun setFilter(newList: List<UserModel>) {
        userModels = ArrayList()
        userModels!!.addAll(newList)
        notifyDataSetChanged()
    }
}
