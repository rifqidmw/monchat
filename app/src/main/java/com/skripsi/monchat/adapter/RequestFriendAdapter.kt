package com.skripsi.monchat.adapter

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.skripsi.monchat.activity.ChatActivity
import com.skripsi.monchat.R
import com.skripsi.monchat.activity.RequestFriendActivity
import com.skripsi.monchat.model.UserModel
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.Handle
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_update_email.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

import java.util.ArrayList

class RequestFriendAdapter(private val mContext: Context, private var userModels: MutableList<UserModel>) :
    RecyclerView.Adapter<RequestFriendAdapter.MyViewHolder>() {

    private lateinit var itemView: View
    private lateinit var session: Session
    private lateinit var cBack: Callback<ResponseBody>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_request_friend, parent, false)

        session = Session(mContext)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val userModel = userModels!![position]
        holder.tvName.text = userModel.name
        holder.tvEmail.text = userModel.email

        holder.btnAccept.setOnClickListener {
            val dialog1 = Dialog(mContext)
            dialog1.setContentView(R.layout.dialog_accept)
            dialog1.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            dialog1.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val btnYes: Button
            val btnCancel: Button

            btnYes = dialog1.findViewById(R.id.button_yes)
            btnCancel = dialog1.findViewById(R.id.button_cancel)

            btnYes.setOnClickListener {
                updateRequestFriend(userModel.username, position)
                dialog1.dismiss()
            }

            btnCancel.setOnClickListener { dialog1.dismiss() }

            dialog1.show()
        }

    }

    fun updateRequestFriend(username: String, position: Int){
        val call = ParamReq.requestUpdateRequestFriendSQL(session.get("username")!!, username, mContext)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleAcceptFriendSQL(response.body()!!.string(), mContext)
                    if (handle) {
                        userModels.removeAt(position)
                        Toast.makeText(mContext, "Berhasil menerima teman", Toast.LENGTH_LONG).show()
                        notifyDataSetChanged()
                    } else {
                        Toast.makeText(mContext, "Gagal Terhubung ke MySQL", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(mContext, "Tidak ada Request Teman", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(mContext, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(mContext, call, cBack, false, "Loading")
    }

    override fun getItemCount(): Int {
        return userModels!!.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvName: TextView
        internal var tvEmail: TextView
        internal var btnAccept: Button

        init {

            tvName = itemView.findViewById(R.id.tv_name)
            tvEmail = itemView.findViewById(R.id.tv_email)
            btnAccept = itemView.findViewById(R.id.btn_accept)

        }
    }


    fun setFilter(newList: List<UserModel>) {
        userModels = ArrayList()
        userModels!!.addAll(newList)
        notifyDataSetChanged()
    }
}
