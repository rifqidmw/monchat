package com.skripsi.monchat.utils

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.skripsi.monchat.activity.ChatActivity
import com.skripsi.monchat.activity.MainActivity
import com.skripsi.monchat.R
import com.skripsi.monchat.model.ChatModel
import io.reactivex.disposables.Disposable
import org.jivesoftware.smack.*
import org.jivesoftware.smack.SmackException.NotConnectedException
import org.jivesoftware.smack.chat.ChatManager
import org.jivesoftware.smack.chat.ChatManagerListener
import org.jivesoftware.smack.chat.ChatMessageListener
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import org.jivesoftware.smackx.muc.MultiUserChat
import org.jivesoftware.smackx.muc.MultiUserChatManager
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager.AutoReceiptMode

import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class MyXMPP(
    internal var context: MyService, private val serverAddress: String, logiUser: String,
    passwordser: String
)  {
    var loggedin = false
    private var chat_created = false
    lateinit var gson: Gson

    var usersList = ArrayList<HashMap<String, String>>()

    var connected = false
    var isconnecting = false
    var isToasted = true
    lateinit var connection: XMPPTCPConnection
    lateinit var loginUser: String
    lateinit var passwordUser: String

    //individual chat
    lateinit var Mychat: org.jivesoftware.smack.chat.Chat
    lateinit var mChatManagerListener: ChatManagerListenerImpl
    lateinit var mMessageListener: MMessageListener

    //group chat


    internal var text = ""
    init {
        try {
            Class.forName("org.jivesoftware.smack.ReconnectionManager")
        } catch (ex: ClassNotFoundException) {
            // problem loading reconnection manager
        }
        this.loginUser = logiUser
        this.passwordUser = passwordser
        init()
    }

    fun init() {
        gson = Gson()
        mMessageListener = MMessageListener(context)
        mChatManagerListener = ChatManagerListenerImpl()
        initialiseConnection()
    }


    //inisialisasi koneksi ke openfire
    private fun initialiseConnection() {
        val config = XMPPTCPConnectionConfiguration.builder()
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
        config.setServiceName(serverAddress)
        config.setHost(serverAddress)
        config.setPort(5222)
        config.setDebuggerEnabled(true)
        XMPPTCPConnection.setUseStreamManagementResumptiodDefault(true)
        XMPPTCPConnection.setUseStreamManagementDefault(true)
        connection = XMPPTCPConnection(config.build())

        val connectionListener = XMPPConnectionListener()
        connection.addConnectionListener(connectionListener)

    }

    //fungsi untuk koneksi ke openfire
    fun connect(caller: String) {
        @SuppressLint("StaticFieldLeak") val connectionThread = object : AsyncTask<Void, Void, Boolean>() {
            @Synchronized
            override fun doInBackground(vararg arg0: Void): Boolean? {
                if (connection.isConnected)
                    return false
                isconnecting = true
                if (isToasted)
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(
                            context,
                            "$caller=>connecting....",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                Log.d("Connect() Function", "$caller=>connecting....")

                //mencoba untuk client konek ke openfire
                try {
                    connection.connect()
                    val dm = DeliveryReceiptManager
                        .getInstanceFor(connection)
                    dm.autoReceiptMode = AutoReceiptMode.always
                    dm.addReceiptReceivedListener { fromid, toid, msgid, packet -> }
                    connected = true

                } catch (e: IOException) {
                    if (isToasted)
                        Handler(Looper.getMainLooper())
                            .post { Toast.makeText(context, "($caller)IOException: ", Toast.LENGTH_SHORT).show() }

                    Log.e("($caller)", "IOException: " + e.message)
                } catch (e: SmackException) {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(
                            context,
                            "($caller)SMACKException: ",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    Log.e("($caller)", "SMACKException: " + e.message)
                } catch (e: XMPPException) {
                    if (isToasted)
                        Handler(Looper.getMainLooper())
                            .post { Toast.makeText(context, "($caller)XMPPException: ", Toast.LENGTH_SHORT).show() }
                    Log.e("connect($caller)", "XMPPException: " + e.message)
                }

                isconnecting = false
                return isconnecting
            }
        }
        connectionThread.execute()
    }

    fun login() {
        try {
            connection.login(loginUser, passwordUser)
            Log.i("LOGIN", "Yey! We're connected to the Xmpp server!")
        } catch (e: XMPPException) {
            e.printStackTrace()
        } catch (e: SmackException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception) {
        }

    }

    inner class ChatManagerListenerImpl : ChatManagerListener {
        override fun chatCreated(
            chat: org.jivesoftware.smack.chat.Chat,
            createdLocally: Boolean
        ) {
            if (!createdLocally)
                chat.addMessageListener(mMessageListener)
        }

    }

    //fungsi untuk mengirim pesan
    fun sendMessage(chatModel: ChatModel) {
        //mengecek apaka pesan sudah dibuat, jika belum maka
        if (!chat_created) {
            Mychat = ChatManager.getInstanceFor(connection).createChat(
                chatModel.receiver + "@"
                        + context.getString(R.string.server_host),
                mMessageListener
            )
            Log.d("receiver123", chatModel.receiver)
            chat_created = true
        }
        val message = Message()
        message.body = chatModel.body
        message.type = Message.Type.normal

        try {
            if (connection.isAuthenticated) {
                //mengirim pesan ke openfire
                Mychat.sendMessage(message)
            } else {
                login()
            }
        } catch (e: NotConnectedException) {
            Log.e("xmpp.SendMessage()", "msg Not sent!-Not Connected!")

        } catch (e: Exception) {
            Log.e("xmpp.SendMessage", "msg Not sent!" + e.message)
        }

    }

    //fungsi yg aktif di backgroun untuk mengecek kondisi client apakah terkoneksi dengan openfire
    inner class XMPPConnectionListener : ConnectionListener {
        override fun connected(connection: XMPPConnection) {
            Log.d("xmpp", "Connected!")
            connected = true
            if (!connection.isAuthenticated) {
                login()
            }
        }

        override fun connectionClosed() {
            if (isToasted)

                Handler(Looper.getMainLooper()).post {
                    Toast.makeText(
                        context, "ConnectionCLosed!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            Log.d("xmpp", "ConnectionCLosed!")
            connected = false
            chat_created = false
            loggedin = false
        }

        override fun connectionClosedOnError(arg0: Exception) {
            if (isToasted)

                Handler(Looper.getMainLooper()).post {
                    Toast.makeText(
                        context, "ConnectionClosedOn Error!!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            Log.d("xmpp", "ConnectionClosedOn Error!")
            connected = false

            chat_created = false
            loggedin = false
        }

        override fun reconnectingIn(arg0: Int) {

            Log.d("xmpp", "Reconnectingin $arg0")

            loggedin = false
        }

        override fun reconnectionFailed(arg0: Exception) {
            if (isToasted)

                Handler(Looper.getMainLooper()).post {
                    Toast.makeText(
                        context, "ReconnectionFailed!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            Log.d("xmpp", "ReconnectionFailed!")
            connected = false

            chat_created = false
            loggedin = false
        }

        override fun reconnectionSuccessful() {
            if (isToasted)

                Handler(Looper.getMainLooper()).post {
                    Toast.makeText(
                        context, "REConnected!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            Log.d("xmpp", "ReconnectionSuccessful")
            connected = true

            chat_created = false
            loggedin = false
        }

        override fun authenticated(arg0: XMPPConnection, arg1: Boolean) {
            Log.d("xmpp", "Authenticated!")
            loggedin = true
            ChatManager.getInstanceFor(connection).addChatListener(
                mChatManagerListener
            )

            chat_created = false
            Thread(Runnable {
                try {
                    Thread.sleep(500)
                } catch (e: InterruptedException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
            }).start()
            if (isToasted)

                Handler(Looper.getMainLooper()).post {
                    // TODO Auto-generated method stub

                    Toast.makeText(
                        context, "Connected!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
        }
    }

    //fungsi untuk menerima pesan dari server
    inner class MMessageListener(contxt: Context) : ChatMessageListener {

        override fun processMessage(
            chat: org.jivesoftware.smack.chat.Chat,
            message: Message
        ) {
            Log.i("MyXMPP_MESSAGE_LISTENER", ("Xmpp message received: '$message"))

            println("Body-----" + message.body)
            println("Subject-----" + message.subject)
            println("FROM-----" + message.from)
            println("TO-----" + message.to
            )

            //mengecek apakah pesan tersebut bertipe chat text dan isi dari pesan tidak kosong
            if ((message.type == Message.Type.chat && message.body != null)) {
                var sender = message.from.split("@")[0] //mengambil nama pengirim
                var receiver = message.to.split("@")[0] //mengambil nama penerima

                //menyimpan data pesan ke model
                val chatModel = ChatModel()
                chatModel.body = message.body
                chatModel.senderName = sender
                chatModel.receiver = receiver
                chatModel.isMine = "2"
                Log.d("receiver123456", chatModel.body)
                processMessage(chatModel)
            }
        }

        private fun processMessage(chatModel: ChatModel) {
//            MainActivity.pushNotification()
            //penyimpan pesan yg masuk ke sqlite database
            val sqliteHelper = SQLiteHelper(context)
            sqliteHelper.addChat(chatModel.senderName, chatModel.receiver, chatModel.body, chatModel.isMine)

            //saat pesan masuk maka list akan reload supaya pesan yg masuk langsung dapat terlihat
            Handler(Looper.getMainLooper()).post {
                val chatActivity = ChatActivity()
                chatActivity.receiveMessage(chatModel)
                ChatActivity.chatAdapter.notifyDataSetChanged()
            }

            //saat pesan masuk maka menjalankan local notification
            val intent = Intent()
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
            val notification = Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Monchat")
                .setContentText("${chatModel.senderName}: ${chatModel.body}")
            notification.setContentIntent(pendingIntent)

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(1,notification.build())
        }

    }

    companion object {
        var instance: MyXMPP? = null
        var instanceCreated = false

        fun getInstance(
            context: MyService, server: String,
            user: String, pass: String
        ): MyXMPP {

            if (instance == null) {
                instance = MyXMPP(context, server, user, pass)
                instanceCreated = true
            }
            return instance!!

        }
    }
}