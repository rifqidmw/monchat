package com.skripsi.monchat.utils

import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.IBinder
import android.util.Log
import com.skripsi.monchat.R

//class untuk login ke openfire
class MyService : Service() {

    lateinit var cm: ConnectivityManager
    //memanggil class MyXMPP untuk dapat memakai fungsi yg ada di class MyXMPP digunakan disini
    lateinit var xmpp: MyXMPP

    override fun onBind(intent: Intent): IBinder? {
        return LocalBinder(this)
    }


    override fun onCreate() {
        super.onCreate()
        val session = Session(applicationContext)
        cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        Log.d("username123", session.get("username")!!)
        //login ke openfire
        xmpp = MyXMPP.getInstance(
            this@MyService,
            getString(R.string.server),
            session.get("username")!!,
            session.get("password")!!
        )
        //client konek ke openfire
        xmpp.connect("onCreate")
    }

    override fun onStartCommand(
        intent: Intent, flags: Int,
        startId: Int
    ): Int {
        return Service.START_NOT_STICKY
    }

    override fun onUnbind(intent: Intent): Boolean {
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        xmpp.connection.disconnect()
    }

}