package com.skripsi.monchat.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

object DateTime {
    private val dateFormat = SimpleDateFormat("d MMM yyyy")
    private val timeFormat = SimpleDateFormat("K:mma")

    val currentTime: String
        get() {

            val today = Calendar.getInstance().time
            return timeFormat.format(today)
        }

    val currentDate: String
        get() {

            val today = Calendar.getInstance().time
            return dateFormat.format(today)
        }

}
