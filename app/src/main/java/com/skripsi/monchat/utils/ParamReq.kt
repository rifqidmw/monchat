package com.skripsi.monchat.utils

import android.annotation.SuppressLint
import android.content.Context
import com.skripsi.monchat.activity.MainActivity
import com.skripsi.monchat.R
import com.skripsi.monchat.request.RequestRegister
import com.skripsi.monchat.request.RequestUpdateEmail
import com.skripsi.monchat.request.RequestUpdateName
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

import java.util.HashMap


/**
 * Created by palapabeta on 02/02/18.
 */

@SuppressLint("StaticFieldLeak")
object ParamReq {

    //Openfire
    var context: Context? = null
    private var APIInterface: Interface? = null

    fun monchat(): MainActivity {
        return context as MainActivity
    }

    fun requestLogin(username: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        return APIInterface!!.requestLogin(context.getString(R.string.token), username)
    }

    fun requestRegister(
        username: String, password: String, name: String,
        email: String , context: Context
    ): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val requstRegister = RequestRegister()
        requstRegister.username = username
        requstRegister.password = password
        requstRegister.name = name
        requstRegister.email = email
        return APIInterface!!.requestRegister(context.getString(R.string.token), requstRegister)
    }

    fun requestUserAll(context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        return APIInterface!!.getUserAll(context.getString(R.string.token))
    }

    fun requestUserDetail(username: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        return APIInterface!!.requestUserDetail(context.getString(R.string.token), username)
    }

    fun requestUpdateName(username: String, name: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val requestUpdateName = RequestUpdateName()
        requestUpdateName.name = name
        return APIInterface!!.requestUpdateName(context.getString(R.string.token), username, requestUpdateName)
    }

    fun requestUpdateEmail(username: String, email: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val requestUpdateEmail = RequestUpdateEmail()
        requestUpdateEmail.email = email
        return APIInterface!!.requestUpdateEmail(context.getString(R.string.token), username, requestUpdateEmail)
    }

    //SQL
    fun requestLoginSQL(username: String, password: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val map = HashMap<String, RequestBody>()

        map["username"] = RequestBody.create(MediaType.parse("multipart/form-data"), username)
        map["password"] = RequestBody.create(MediaType.parse("multipart/form-data"), password)
        return APIInterface!!.requestLoginSQL(map)
    }

    fun requesRegisterSQL(username: String, fullname: String, email: String, password: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val map = HashMap<String, RequestBody>()

        map["username"] = RequestBody.create(MediaType.parse("multipart/form-data"), username)
        map["fullname"] = RequestBody.create(MediaType.parse("multipart/form-data"), fullname)
        map["email"] = RequestBody.create(MediaType.parse("multipart/form-data"), email)
        map["password"] = RequestBody.create(MediaType.parse("multipart/form-data"), password)
        return APIInterface!!.requestRegisterSQL(map)
    }

    fun requestUpdateNameSQL(username: String, name: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val map = HashMap<String, RequestBody>()

        map["username"] = RequestBody.create(MediaType.parse("multipart/form-data"), username)
        map["fullname"] = RequestBody.create(MediaType.parse("multipart/form-data"), name)
        return APIInterface!!.requestUpdateNameSQL(map)
    }

    fun requestUpdateEmailSQL(username: String, email: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val map = HashMap<String, RequestBody>()

        map["username"] = RequestBody.create(MediaType.parse("multipart/form-data"), username)
        map["email"] = RequestBody.create(MediaType.parse("multipart/form-data"), email)
        return APIInterface!!.requestUpdateEmailSQL(map)
    }

    fun requestUpdateRequestFriendSQL(username: String, username_friend: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val map = HashMap<String, RequestBody>()

        map["username"] = RequestBody.create(MediaType.parse("multipart/form-data"), username)
        map["username_friend"] = RequestBody.create(MediaType.parse("multipart/form-data"), username_friend)
        return APIInterface!!.requestUpdateRequestFriendSQL(map)
    }

    fun requestFriendListSQL(username: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val map = HashMap<String, RequestBody>()

        map["username"] = RequestBody.create(MediaType.parse("multipart/form-data"), username)
        return APIInterface!!.requestFriendListSQL(map)
    }

    fun requestInviteFriendSQL(username: String, username_friend: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val map = HashMap<String, RequestBody>()

        map["username"] = RequestBody.create(MediaType.parse("multipart/form-data"), username)
        map["username_friend"] = RequestBody.create(MediaType.parse("multipart/form-data"), username_friend)
        return APIInterface!!.requestInviteFriendSQL(map)
    }

    fun requestInviteFriendListSQL(username: String, context: Context): Call<ResponseBody> {
        APIInterface = Api.initRetrofit(Api.showLog)
        val map = HashMap<String, RequestBody>()

        map["username"] = RequestBody.create(MediaType.parse("multipart/form-data"), username)
        return APIInterface!!.requestInviteFriendListSQL(map)
    }



}
