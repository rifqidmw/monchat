package com.skripsi.monchat.utils

import com.skripsi.monchat.request.RequestCreateGroup
import com.skripsi.monchat.request.RequestRegister
import com.skripsi.monchat.request.RequestUpdateEmail
import com.skripsi.monchat.request.RequestUpdateName
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by XGibar on 27/10/2016.
 */
interface Interface {


    //Openfire
    @Headers("Accept: application/json")
    @GET("http://172.27.13.29:9090/plugins/restapi/v1/users/{id}")
    fun requestLogin(@Header("Authorization") authorization: String, @Path("id") username: String): Call<ResponseBody>

    @Headers("Accept: application/json")
    @POST("http://172.27.13.29:9090/plugins/restapi/v1/users")
    fun requestRegister(@Header("Authorization") authorization: String, @Body requestRegister: RequestRegister): Call<ResponseBody>

    @Headers("Accept: application/json")
    @GET("http://172.27.13.29:9090/plugins/restapi/v1/users/{id}")
    fun requestUserDetail(@Header("Authorization") authorization: String, @Path("id") username: String): Call<ResponseBody>

    @Headers("Accept: application/json")
    @GET("http://172.27.13.29:9090/plugins/restapi/v1/users")
    fun getUserAll(@Header("Authorization") authorization: String): Call<ResponseBody>

    @Headers("Accept: application/json")
    @PUT("http://172.27.13.29:9090/plugins/restapi/v1/users/{id}")
    fun requestUpdateName(@Header("Authorization") authorization: String, @Path("id") username: String, @Body requestUpdateName: RequestUpdateName): Call<ResponseBody>

    @Headers("Accept: application/json")
    @PUT("http://172.27.13.29:9090/plugins/restapi/v1/users/{id}")
    fun requestUpdateEmail(@Header("Authorization") authorization: String, @Path("id") username: String, @Body requestUpdateEmail: RequestUpdateEmail): Call<ResponseBody>

    @Headers("Accept: application/json")
    @POST("http://172.27.13.29:9090/plugins/restapi/v1/chatrooms")
    fun requestCreateGroup(@Header("Authorization") authorization: String, @Body requestCreateGroup: RequestCreateGroup): Call<ResponseBody>

    //SQL
    @Multipart
    @POST("http://172.27.13.29/monchat/login.php")
    fun requestLoginSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>

    @Multipart
    @POST("http://172.27.13.29/monchat/register.php")
    fun requestRegisterSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>

    @Multipart
    @POST("http://172.27.13.29/monchat/updatename.php")
    fun requestUpdateNameSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>

    @Multipart
    @POST("http://172.27.13.29/monchat/updateemail.php")
    fun requestUpdateEmailSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>

    @Multipart
    @POST("http://172.27.13.29/monchat/acceptfriend.php")
    fun requestUpdateRequestFriendSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>

    @Multipart
    @POST("http://172.27.13.29/monchat/getfriend.php")
    fun requestFriendListSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>

    @Multipart
    @POST("http://172.27.13.29/monchat/invite.php")
    fun requestInviteFriendSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>

    @Multipart
    @POST("http://172.27.13.29/monchat/requestfriend.php")
    fun requestInviteFriendListSQL(@PartMap params: HashMap<String, RequestBody>): Call<ResponseBody>
}


