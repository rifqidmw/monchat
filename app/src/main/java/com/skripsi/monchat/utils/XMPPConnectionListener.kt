package com.skripsi.monchat.utils

import android.util.Log
import org.jivesoftware.smack.ConnectionListener
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.chat.ChatManagerListener
import java.lang.Exception
import org.jivesoftware.smack.chat.ChatManager



class XMPPConnectionListener :  ConnectionListener{

    lateinit var mChatManagerListener: ChatManagerListener
    override fun connected(connection: XMPPConnection?) {
        Log.d("xmpp", "Connected!")
    }

    override fun connectionClosed() {
        Log.d("xmpp", "ConnectionCLosed!")

    }

    override fun connectionClosedOnError(e: Exception?) {
        Log.d("xmpp", "ConnectionClosedOn Error!")

    }

    override fun reconnectionSuccessful() {
        Log.d("xmpp", "ReconnectionSuccessful")

    }

    override fun authenticated(connection: XMPPConnection?, resumed: Boolean) {
        Log.d("xmpp", "Authenticated!")


        ChatManager.getInstanceFor(connection).addChatListener(mChatManagerListener)

        Thread(Runnable {
            try {
                Thread.sleep(500)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }).start()


        Log.e("xmpp", "Conected")
    }

    override fun reconnectionFailed(e: Exception?) {
        Log.d("xmpp", "Reconnectingin $e")

    }

    override fun reconnectingIn(seconds: Int) {
        Log.d("xmpp", "Reconnectingin " + seconds)
    }
}