package com.skripsi.monchat.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.skripsi.monchat.R
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.Handle
import com.skripsi.monchat.utils.ParamReq
import kotlinx.android.synthetic.main.activity_register.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

class RegisterActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this.finish()
        }
        btn_daftar.setOnClickListener {
            registerSQL()
        }
    }

    fun registerSQL(){
        //mengecek supaya tidak ada form yg kosong
        if (!et_username.text.toString().isEmpty() && !et_nama.text.toString().isEmpty() &&
                !et_email.text.toString().isEmpty() && !et_password.text.toString().isEmpty()){

            //mengecek jumlah karakter dari password tidak boleh kurang dari 6
            if (et_password.length() < 6){
                Toast.makeText(applicationContext, "Password minimal 6 karakter", Toast.LENGTH_LONG).show()
            } else {
                val callSQL = ParamReq.requesRegisterSQL(et_username.text.toString() , et_nama.text.toString() ,
                    et_email.text.toString() , et_password.text.toString(),applicationContext)
                cBack = object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                        try {
                            val handle = Handle.handleRegisterSQL(response.body()!!.string(), applicationContext)
                            if (handle) {
                                registerOpenfire()
                            } else {

                            }
                        } catch (e: Exception) {
                            Toast.makeText(applicationContext, "Username sudah terdaftar", Toast.LENGTH_LONG).show()
                            e.printStackTrace()
                        }
                    }
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Api.retryDialog(applicationContext, call, cBack, 1, false)
                    }
                }
                Api.enqueueWithRetry(applicationContext, callSQL, cBack, false, "Loading")
            }
        } else{
            Toast.makeText(applicationContext, "Harap lengkapi semua data terlebih dahulu", Toast.LENGTH_LONG).show()
        }
    }

    fun registerOpenfire(){
        val call = ParamReq.requestRegister(et_username.text.toString() , et_password.text.toString() ,
            et_nama.text.toString() , et_email.text.toString(),applicationContext)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleRegister(response.body()!!.string(), applicationContext)
                    if (!handle) {
                        Toast.makeText(this@RegisterActivity, "Register Berhasil", Toast.LENGTH_LONG).show()
                        val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this@RegisterActivity, "Gagal Terhubung ke Openfire", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(applicationContext, "Username sudah terdaftar", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(applicationContext, call, cBack, 1, false)
            }
        }

        Api.enqueueWithRetry(applicationContext, call, cBack, false, "Loading")
    }
}
