package com.skripsi.monchat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skripsi.monchat.R
import com.skripsi.monchat.adapter.AddFriendAdapter
import com.skripsi.monchat.adapter.UserAdapter
import com.skripsi.monchat.model.UserModel
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_add_friend.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import java.util.ArrayList

class AddFriendActivity : AppCompatActivity() {

    private lateinit var adapter: AddFriendAdapter

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_friend)

        //fungsi untuk mengecek isi dari edittext saat diketik
        et_user_search.addTextChangedListener(object : TextWatcher{
            //pada saat text diketik, setiap saat akan melakukan pencocokan data dengan text yg diketik
            override fun afterTextChanged(editable: Editable?) {
                var textSearch = editable.toString()
                textSearch = textSearch.toLowerCase()
                var newList: MutableList<UserModel> = ArrayList<UserModel>()
                if (textSearch.isEmpty()) {
                    newList = Api.userList
                } else {
                    //mengecek data user dengan text yg diketik apakah ada kecocokan
                    for (userList in Api.userList) {
                        val title = userList.username.toLowerCase()
                        if (title.contains(textSearch)) {
                            //jika ada maka dimasukkan ke array
                            newList.add(userList)
                        }
                    }
                }

                //menampilkan hasil pencarian
                adapter.setFilter(newList)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this@AddFriendActivity.finish()
        }

        getUserList()
    }

    //fungsi untuk mendapatkan data semua user
    fun getUserList(){
        val call = ParamReq.requestUserAll(this@AddFriendActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    Api.userList.clear()
                    val jsonObject = JSONObject(response.body()!!.string())

                    val data = jsonObject.getJSONArray("users")
                    if (data.length() >= 0) {
                        Log.d("data123", Integer.toString(data.length()))
                        for (i in 0 until data.length()) {
                            val user = UserModel()
                            val session = Session(this@AddFriendActivity)
                            if (data.getJSONObject(i).getString("username") != session.get("username")){
                                user.name =data.getJSONObject(i).getString("name")
                                user.username = data.getJSONObject(i).getString("username")
                                user.email = data.getJSONObject(i).getString("email")
                                Api.userList.add(user)
                            }
                        }
                    } else {
                        Log.d("user", "data not found")
                    }

                    adapter = AddFriendAdapter(this@AddFriendActivity, Api.userList)
                    recycler_view.layoutManager = LinearLayoutManager(
                        this@AddFriendActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                    recycler_view.itemAnimator = DefaultItemAnimator()!!
                    recycler_view.adapter = adapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@AddFriendActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@AddFriendActivity, call, cBack, false, "Loading")
    }
}
