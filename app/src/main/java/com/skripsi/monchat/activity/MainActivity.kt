package com.skripsi.monchat.activity

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.skripsi.monchat.R
import com.skripsi.monchat.adapter.RequestFriendAdapter
import com.skripsi.monchat.adapter.UserAdapter
import com.skripsi.monchat.fragment.ChatFragment
import com.skripsi.monchat.fragment.GroupFragment
import com.skripsi.monchat.model.UserModel
import com.skripsi.monchat.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_request_friend.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import org.jivesoftware.smack.roster.Roster
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session
    private lateinit var roster: Roster
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager


    private val TAG = "MonChat"
    private var mBounded: Boolean = false


    //variabel public
    companion object{
        var mService: MyService? = null
        lateinit var context: Context

        //menjalankan class MyService
        fun getmService(): MyService {
            return mService!!
        }

    }

    //ngecek koneksi ke openfire
    private val mConnection = object : ServiceConnection {

        override fun onServiceConnected(
            name: ComponentName,
            service: IBinder
        ) {
            mService = (service as LocalBinder<MyService>).service
            mBounded = true
            Log.d(TAG, "onServiceConnected")
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBounded = false
            Log.d(TAG, "onServiceDisconnected")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        doBindService()

        context = this@MainActivity

        session = Session(this@MainActivity)
        session.save("inchatroom", "2")

        getUserDetail()
        getRequestFriend()

        btn_profile.setOnClickListener {
            startActivity(Intent(this@MainActivity, ProfileActivity::class.java))
        }

        //view pager itu fragment
        viewPager = findViewById(R.id.viewpager) as ViewPager
        setupViewPager(viewPager)

        //tab
        tabLayout = findViewById(R.id.tabs) as TabLayout
        tabLayout.setupWithViewPager(viewPager)
    }

    //setup tab untuk nama tab dan tujuan tab
    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(ChatFragment(), "Chat")
        adapter.addFragment(GroupFragment(), "Group")
        viewPager.adapter = adapter
    }

    //adapter buat tab
    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        override fun getCount(): Int {
            return mFragmentList.size
        }

        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    public override fun onResume() {
        super.onResume()
        getUserDetail()
        getRequestFriend()
    }

    override fun onDestroy() {
        super.onDestroy()
        doUnbindService()
    }

    internal fun doBindService() {
        bindService(
            Intent(this, MyService::class.java), mConnection,
            Context.BIND_AUTO_CREATE
        )
    }

    internal fun doUnbindService() {
        if (mConnection != null) {
            unbindService(mConnection)
        }
    }

    //fungsi untuk mengecek apakah ada permintaan teman masuk
    fun getRequestFriend(){
        val call = ParamReq.requestInviteFriendListSQL(session.get("username")!!,this@MainActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    Api.userList.clear()
                    val jsonObject = JSONObject(response.body()!!.string())

                    val data = jsonObject.getJSONArray("data")
                    if (data.length() > 0) {
                        Log.d("data123", Integer.toString(data.length()))

                        //jika ada maka akan menampilkan text jumlah permintaan teman yang masuk
                        tv_notif.text = Integer.toString(data.length())
                    } else {
                        tv_notif.visibility = View.INVISIBLE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@MainActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@MainActivity, call, cBack, false, "Loading")
    }

    //fungsi untuk mendapatkan nama user dari server
    fun getUserDetail(){
        val call = ParamReq.requestUserDetail(session.get("username")!!,this@MainActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val jsonObject = JSONObject(response.body()!!.string())

                    tv_name.text = jsonObject.getString("name")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@MainActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@MainActivity, call, cBack, false, "Loading")
    }

}
