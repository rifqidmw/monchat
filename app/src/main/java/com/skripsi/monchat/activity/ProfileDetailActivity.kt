package com.skripsi.monchat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.skripsi.monchat.R
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_profile_detail.*
import kotlinx.android.synthetic.main.activity_profile_detail.toolbar
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

class ProfileDetailActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_detail)

        session = Session(this@ProfileDetailActivity)

        getUserDetail()

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this.finish()
        }
    }

    public override fun onResume() {  // After a pause OR at startup
        super.onResume()
        getUserDetail()
    }

    fun getUserDetail(){
        val call = ParamReq.requestUserDetail(session.get("username")!!,this@ProfileDetailActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val jsonObject = JSONObject(response.body()!!.string())
                    et_nama.setText(jsonObject.getString("name"))
                    et_email.setText(jsonObject.getString("email"))
                    et_username.setText(jsonObject.getString("username"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@ProfileDetailActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@ProfileDetailActivity, call, cBack, false, "Loading")
    }
}
