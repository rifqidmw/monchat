package com.skripsi.monchat.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.skripsi.monchat.R
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_profile_pengaturan.*
import kotlinx.android.synthetic.main.activity_profile_pengaturan.toolbar
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

class ProfilePengaturanActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_pengaturan)

        session = Session(this@ProfilePengaturanActivity)

        getUserDetail()

        btn_ubah_nama.setOnClickListener {
            startActivity(Intent(this@ProfilePengaturanActivity, UpdateNameActivity::class.java))
        }

        btn_ubah_email.setOnClickListener {
            startActivity(Intent(this@ProfilePengaturanActivity, UpdateEmailActivity::class.java))
        }

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this.finish()
        }
    }

    public override fun onResume() {  // After a pause OR at startup
        super.onResume()
        getUserDetail()
    }

    fun getUserDetail(){
        val call = ParamReq.requestUserDetail(session.get("username")!!,this@ProfilePengaturanActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val jsonObject = JSONObject(response.body()!!.string())
                    tv_name.text = jsonObject.getString("name")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@ProfilePengaturanActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@ProfilePengaturanActivity, call, cBack, false, "Loading")
    }
}
