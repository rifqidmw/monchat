package com.skripsi.monchat.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.skripsi.monchat.R
import com.skripsi.monchat.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

class LoginActivity : AppCompatActivity() {

    //inisialisasi retrofit untuk menerima respon
    private lateinit var cBack: Callback<ResponseBody>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //inisialisasi session
        val session = Session(applicationContext)

        //ngecek apakah session masih ada
        try {
            //ngecek session dengan paramter username
            if (!session.get("username").isNullOrEmpty()) {
                //jika ada maka langsung masuk ke MainActivity
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
            }
        } catch (e: Exception) {

        }

        btn_login.setOnClickListener {
            loginSQL()
        }

        btn_daftar.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }
    fun loginSQL(){
        //request buat login ke server mysql
        val call = ParamReq.requestLoginSQL(et_username.text.toString(), et_password.text.toString(), applicationContext)
        //jika sudah request maka mengambil respon dari server
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    //jika request ke server berhasil
                    val handle = Handle.handleLoginSQL(response.body()!!.string(), et_password.text.toString(), applicationContext)
                    if (handle) {
                        //login ke openfire
                        loginOpenfire()
                    } else {
                        Toast.makeText(this@LoginActivity, "Gagal Terhubung ke MySQL", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(applicationContext, "Username belum terdaftar", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(applicationContext, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(applicationContext, call, cBack, false, "Loading")
    }

    fun loginOpenfire(){
        //request buat login ke server openfire
        val call = ParamReq.requestLogin(et_username.text.toString(), applicationContext)
        //jika sudah request maka mengambil respon dari server
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleLogin(response.body()!!.string(), applicationContext)
                    if (!handle) {
                        Toast.makeText(this@LoginActivity, "Gagal Terhubung ke Openfire", Toast.LENGTH_LONG).show()
                    } else {
                        //masuk ke file Handler, disana kalo ternyata username ada maka langsung masuk ke MainActivity
                    }
                } catch (e: Exception) {
                    Toast.makeText(applicationContext, "Username belum terdaftar", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(applicationContext, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(applicationContext, call, cBack, false, "Loading")
    }
}
