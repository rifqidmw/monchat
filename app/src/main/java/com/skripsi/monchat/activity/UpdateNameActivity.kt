package com.skripsi.monchat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.skripsi.monchat.R
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.Handle
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_update_name.*
import kotlinx.android.synthetic.main.activity_update_name.toolbar
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

class UpdateNameActivity : AppCompatActivity() {

    lateinit var session: Session
    private lateinit var cBack: Callback<ResponseBody>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_name)

        session = Session(this@UpdateNameActivity)

        getUserDetail()

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this.finish()
        }

        btn_confirm.setOnClickListener {
            updateNameSQL()
        }
    }

    fun getUserDetail(){
        val call = ParamReq.requestUserDetail(session.get("username")!!,this@UpdateNameActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val jsonObject = JSONObject(response.body()!!.string())
                    et_nama_lama.setText(jsonObject.getString("name"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@UpdateNameActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@UpdateNameActivity, call, cBack, false, "Loading")
    }

    fun updateNameSQL(){

        if (!et_nama_baru.text.isNullOrEmpty()){
            val call = ParamReq.requestUpdateNameSQL(session.get("username")!!, et_nama_baru.text.toString(), applicationContext)
            cBack = object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                    try {
                        val handle = Handle.handleUpdateNameSQL(response.body()!!.string(), applicationContext)
                        if (handle) {
                            updateNameOpenfire()
                        } else {
                            Toast.makeText(this@UpdateNameActivity, "Gagal Terhubung ke MySQL", Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        Toast.makeText(applicationContext, "Username belum terdaftar", Toast.LENGTH_LONG).show()
                        e.printStackTrace()
                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Api.retryDialog(applicationContext, call, cBack, 1, false)
                }
            }
            Api.enqueueWithRetry(applicationContext, call, cBack, false, "Loading")
        } else {
            Toast.makeText(applicationContext, "Nama baru harus diisi", Toast.LENGTH_LONG).show()
        }
    }

    fun updateNameOpenfire(){
        val call = ParamReq.requestUpdateName(session.get("username")!!, et_nama_baru.text.toString(), applicationContext)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleUpdateName(response.body()!!.string(), applicationContext)
                    if (!handle) {
                        Toast.makeText(this@UpdateNameActivity, "Ubah Nama Berhasil", Toast.LENGTH_LONG).show()
                        onBackPressed()
                        this@UpdateNameActivity.finish()
                    } else {
                        Toast.makeText(this@UpdateNameActivity, "Gagal Terhubung ke Openfire", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(applicationContext, "Username belum terdaftar", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(applicationContext, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(applicationContext, call, cBack, false, "Loading")
    }
}
