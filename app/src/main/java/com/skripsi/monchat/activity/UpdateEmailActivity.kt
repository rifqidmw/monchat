package com.skripsi.monchat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.skripsi.monchat.R
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.Handle
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_update_email.*
import kotlinx.android.synthetic.main.activity_update_email.toolbar
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

class UpdateEmailActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>
    lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_email)

        session = Session(this@UpdateEmailActivity)

        getUserDetail()

        btn_confirm.setOnClickListener {
            updateEmailSQL()
        }

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this.finish()
        }
    }

    fun getUserDetail(){
        val call = ParamReq.requestUserDetail(session.get("username")!!,this@UpdateEmailActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val jsonObject = JSONObject(response.body()!!.string())
                    et_email_lama.setText(jsonObject.getString("email"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@UpdateEmailActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@UpdateEmailActivity, call, cBack, false, "Loading")
    }

    fun updateEmailSQL(){

        if (!et_email_baru.text.isNullOrEmpty() || !et_email_konfirmasi.text.isNullOrEmpty()){
            if (et_email_baru.text.toString().equals(et_email_konfirmasi.text.toString())){
                val call = ParamReq.requestUpdateEmailSQL(session.get("username")!!, et_email_baru.text.toString(), applicationContext)
                cBack = object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                        try {
                            val handle = Handle.handleUpdateEmailSQL(response.body()!!.string(), applicationContext)
                            if (handle) {
                                updateEmailOpenfire()
                            } else {
                                Toast.makeText(this@UpdateEmailActivity, "Gagal Terhubung ke MySQL", Toast.LENGTH_LONG).show()
                            }
                        } catch (e: Exception) {
                            Toast.makeText(applicationContext, "Username belum terdaftar", Toast.LENGTH_LONG).show()
                            e.printStackTrace()
                        }

                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Api.retryDialog(applicationContext, call, cBack, 1, false)
                    }
                }
                Api.enqueueWithRetry(applicationContext, call, cBack, false, "Loading")
            } else {
                Toast.makeText(applicationContext, "Email baru dan Email konfirmasi harus sama", Toast.LENGTH_LONG).show()
            }
        } else {
            Toast.makeText(applicationContext, "Email baru dan Email konfirmasi harus diisi", Toast.LENGTH_LONG).show()
        }
    }

    fun updateEmailOpenfire(){
        val call = ParamReq.requestUpdateEmail(session.get("username")!!, et_email_baru.text.toString(), applicationContext)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val handle = Handle.handleUpdateEmail(response.body()!!.string(), applicationContext)
                    if (!handle) {
                        Toast.makeText(this@UpdateEmailActivity, "Ubah Email Berhasil", Toast.LENGTH_LONG).show()
                        onBackPressed()
                        this@UpdateEmailActivity.finish()
                    } else {
                        Toast.makeText(this@UpdateEmailActivity, "Gagal Terhubung ke Openfire", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(applicationContext, "Username belum terdaftar", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(applicationContext, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(applicationContext, call, cBack, false, "Loading")
    }
}
