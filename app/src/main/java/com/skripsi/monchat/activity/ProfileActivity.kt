package com.skripsi.monchat.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.skripsi.monchat.R
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile.toolbar
import kotlinx.android.synthetic.main.activity_profile.tv_name
import kotlinx.android.synthetic.main.activity_profile.tv_notif
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

class ProfileActivity : AppCompatActivity() {

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        session = Session(applicationContext)

        getUserDetail()
        getRequestFriend()

        toolbar.setNavigationOnClickListener {
            startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
            this.finish()
        }

        btn_profil.setOnClickListener {
            startActivity(Intent(this@ProfileActivity, ProfileDetailActivity::class.java))
        }

        btn_request_friend.setOnClickListener {
            startActivity(Intent(this@ProfileActivity, RequestFriendActivity::class.java))
        }

        btn_pengaturan.setOnClickListener {
            startActivity(Intent(this@ProfileActivity, ProfilePengaturanActivity::class.java))
        }

        btn_logout.setOnClickListener {
            session.clear()
            this@ProfileActivity.finish()
            startActivity(Intent(this@ProfileActivity, LoginActivity::class.java))
        }
    }

    public override fun onResume() {  // After a pause OR at startup
        super.onResume()
        getUserDetail()
        getRequestFriend()
    }

    fun getRequestFriend(){
        val call = ParamReq.requestInviteFriendListSQL(session.get("username")!!,this@ProfileActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    Api.userList.clear()
                    val jsonObject = JSONObject(response.body()!!.string())

                    val data = jsonObject.getJSONArray("data")
                    if (data.length() > 0) {
                        Log.d("data123", Integer.toString(data.length()))
                        tv_notif.text = Integer.toString(data.length())
                    } else {
                        tv_notif.visibility = View.INVISIBLE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@ProfileActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@ProfileActivity, call, cBack, false, "Loading")
    }

    fun getUserDetail(){
        val call = ParamReq.requestUserDetail(session.get("username")!!,this@ProfileActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    val jsonObject = JSONObject(response.body()!!.string())

                    tv_name.text = jsonObject.getString("name")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@ProfileActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@ProfileActivity, call, cBack, false, "Loading")
    }
}
