package com.skripsi.monchat.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skripsi.monchat.R
import com.skripsi.monchat.adapter.AddFriendAdapter
import com.skripsi.monchat.adapter.RequestFriendAdapter
import com.skripsi.monchat.model.UserModel
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_request_friend.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

class RequestFriendActivity : AppCompatActivity() {

    private lateinit var adapter: RequestFriendAdapter

    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_friend)

        session = Session(this@RequestFriendActivity)

        toolbar.setNavigationOnClickListener {
            startActivity(Intent(this@RequestFriendActivity, ProfileActivity::class.java))
            this@RequestFriendActivity.finish()
        }
        getUserList()
    }


    fun getUserList(){
        val call = ParamReq.requestInviteFriendListSQL(session.get("username")!!,this@RequestFriendActivity)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    Api.userList.clear()
                    val jsonObject = JSONObject(response.body()!!.string())

                    val data = jsonObject.getJSONArray("data")
                    if (data.length() >= 0) {
                        Log.d("data123", Integer.toString(data.length()))
                        for (i in 0 until data.length()) {
                            val user = UserModel()
                            val session = Session(this@RequestFriendActivity)
                            if (data.getJSONObject(i).getString("username") != session.get("username")){
                                user.name =data.getJSONObject(i).getString("fullname")
                                user.username = data.getJSONObject(i).getString("username")
                                user.email = data.getJSONObject(i).getString("email")
                                Api.userList.add(user)
                            }
                        }
                    } else {
                        Log.d("user", "data not found")
                    }

                    adapter = RequestFriendAdapter(this@RequestFriendActivity, Api.userList)
                    recycler_view.layoutManager = LinearLayoutManager(
                        this@RequestFriendActivity,
                        RecyclerView.VERTICAL,
                        false
                    )
                    recycler_view.itemAnimator = DefaultItemAnimator()!!
                    recycler_view.adapter = adapter
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(this@RequestFriendActivity, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(this@RequestFriendActivity, call, cBack, false, "Loading")
    }
}
