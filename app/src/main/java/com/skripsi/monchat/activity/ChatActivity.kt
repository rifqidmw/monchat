package com.skripsi.monchat.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.skripsi.monchat.R
import com.skripsi.monchat.adapter.ChatAdapter
import com.skripsi.monchat.model.ChatModel
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.DateTime
import com.skripsi.monchat.utils.SQLiteHelper
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.activity_chat.*
import java.util.*
import kotlin.collections.ArrayList

class ChatActivity : AppCompatActivity() {

    private var random: Random? = null
    lateinit var session: Session

    lateinit var userReceiver: String

    lateinit var chatList: MutableList<ChatModel>

    companion object{
        lateinit var chatAdapter: ChatAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        session = Session(this@ChatActivity)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this@ChatActivity.finish()
        }

        random = Random()
        //inisialisasi intent untuk mendapatkan data dari activity sebelumnya
        val intent = intent
        //inisialisasi sqlite database
        val sqliteHelper = SQLiteHelper(this@ChatActivity)
        //mendapatkan data username dari activity sebelumnya
        userReceiver = intent.getStringExtra("USERNAME")!!
        toolbar.title = userReceiver
        toolbar.setTitleTextColor(resources.getColor(R.color.colorWhite))

        //inisialisasi arraylist
        Api.chatList = ArrayList<ChatModel>()

        //arraylist mengambil data chat history dari sqlite database
        Api.chatList.addAll(sqliteHelper.chatHistory(session.get("username")!!, userReceiver))

        val linearLayoutManager = LinearLayoutManager(this@ChatActivity)
        linearLayoutManager.stackFromEnd = true
        linearLayoutManager.reverseLayout = false
        chatAdapter = ChatAdapter(this@ChatActivity, Api.chatList)
        recycler_view.layoutManager = linearLayoutManager
        recycler_view.itemAnimator = DefaultItemAnimator()
        recycler_view.adapter = chatAdapter

        btn_send.setOnClickListener {
            sendMessage()
        }
    }

    //fungsi untuk mengirim pesan
    fun sendMessage(){
        val message = et_message.text.toString()
        //mengecek bahwa pesan tidak boleh kosong
        if (!message.equals("", ignoreCase = true)) {

            //set data pesan
            val chatMessage = ChatModel()
            chatMessage.senderName = session.get("username")!!
            chatMessage.receiver = userReceiver
            chatMessage.body = message
            chatMessage.isMine = "1"
            chatMessage.date = DateTime.currentDate
            chatMessage.time = DateTime.currentTime
            et_message.setText("")

            //menyimpan data pesan ke arraylist
            Api.chatList.add(chatMessage)

            val sqliteHelper = SQLiteHelper(this@ChatActivity)
            //menyimpan data pesan ke sqlite database
            sqliteHelper.addChat(session.get("username")!!, userReceiver, message, "1")

            et_message.setText("")
            //reload list pesan
            chatAdapter.notifyDataSetChanged()
            //perintah untuk mengirim pesan ke server
            MainActivity.getmService().xmpp.sendMessage(chatMessage)
        }
    }

    fun receiveMessage(chatModel: ChatModel){
        Api.chatList.add(chatModel)
    }
}
