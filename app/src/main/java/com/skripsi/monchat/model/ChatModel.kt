package com.skripsi.monchat.model

/**
 * Created by Khushvinders on 15-Nov-16.
 */

import java.util.Random

class ChatModel {

    lateinit var body: String
    lateinit var sender: String
    lateinit var receiver: String
    lateinit var senderName: String
    lateinit var date: String
    lateinit var time: String
    lateinit var msgid: String
    lateinit var isMine: String


    fun setMsgID() {

        msgid += "-" + String.format("%02d", Random().nextInt(100))
    }
}