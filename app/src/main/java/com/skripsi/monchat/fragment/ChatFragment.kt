package com.skripsi.monchat.fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

import com.skripsi.monchat.R
import com.skripsi.monchat.activity.AddFriendActivity
import com.skripsi.monchat.adapter.UserAdapter
import com.skripsi.monchat.model.UserModel
import com.skripsi.monchat.utils.Api
import com.skripsi.monchat.utils.ParamReq
import com.skripsi.monchat.utils.Session
import kotlinx.android.synthetic.main.fragment_chat.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ChatFragment : Fragment() {

    private lateinit var adapter: UserAdapter
    private lateinit var cBack: Callback<ResponseBody>
    private lateinit var session: Session

    private lateinit var recycler_view: RecyclerView
    private lateinit var floatingActionButton: FloatingActionButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_chat, container, false)

        recycler_view = view.findViewById(R.id.recycler_view)
        floatingActionButton = view.findViewById(R.id.btn_add_friend)

        Api.userList = ArrayList<UserModel>()
        adapter = UserAdapter(context!!, Api.userList)
        recycler_view.layoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )
        recycler_view.itemAnimator = DefaultItemAnimator()!!
        recycler_view.adapter = adapter

        floatingActionButton.setOnClickListener {
            startActivity(Intent(context, AddFriendActivity::class.java))
        }

        getUserList()

        return view
    }

    override fun onResume() {
        super.onResume()
        getUserList()
    }

    fun getUserList(){
        val session = Session(context!!)
        val call = ParamReq.requestFriendListSQL(session.get("username")!!,context!!)
        cBack = object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                try {
                    Api.userList.clear()
                    val jsonObject = JSONObject(response.body()!!.string())

                    val data = jsonObject.getJSONArray("data")
                    if (data.length() >= 0) {
                        Log.d("data123", Integer.toString(data.length()))
                        for (i in 0 until data.length()) {
                            val user = UserModel()
                            if (data.getJSONObject(i).getString("username") != session.get("username")){
                                user.name =data.getJSONObject(i).getString("fullname")
                                user.username = data.getJSONObject(i).getString("username")
                                user.email = data.getJSONObject(i).getString("email")
                                Api.userList.add(user)
                            }

                            adapter.notifyDataSetChanged()
                        }
                    } else {
                        Api.userList.clear()
                        adapter.notifyDataSetChanged()
                        Log.d("user", "data not found")
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Api.retryDialog(context!!, call, cBack, 1, false)
            }
        }
        Api.enqueueWithRetry(context!!, call, cBack, false, "Loading")
    }

}
